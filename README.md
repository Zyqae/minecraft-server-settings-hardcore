# MINECRAFT SERVER CONFIG

World is already created with a cake somewhere, just delete the world directory and a new one is going to be created

## MODS INSTALLED
**Hostile Mods Improve Over Time**
Mobs get more difficult the longer the run is
https://modrinth.com/datapack/hostile-mobs-improve-over-time

**Simple Voice Chat**
Introduces a proximity voice chat
*needs to be installed on the client/player-side aswell*
https://modrinth.com/plugin/simple-voice-chat/

**Shared Life**
Adds a shared lifebar, means when one takes dmg, everyone takes dmg, when one heals, everyone heals
https://modrinth.com/datapack/shared-life

**More Mobs**
Adds new heads to mobs so they look different, that's all
https://modrinth.com/datapack/more-mobs

## USAGE

see https://docker-minecraft-server.readthedocs.io/en/latest/

see https://www.docker.com/

just do a `docker compose up -d`
